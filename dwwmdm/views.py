from django.shortcuts import render 
from django.shortcuts import redirect
import pyrebase 

# Config. Pyrebase 
config = {
    "apiKey": "AIzaSyCZo3fhlBEziKSbN_60HCEM2peXsyX4O_Y",
    "authDomain": "dwwm-decision-maker.firebaseapp.com",
    "databaseURL": "https://dwwm-decision-maker.firebaseio.com",
    "projectId": "dwwm-decision-maker",
    "storageBucket": "dwwm-decision-maker.appspot.com",
    "messagingSenderId": "323379121750",
    "appId": "1:323379121750:web:2076927d1b6e6c96e235f7"  
}
firebase = pyrebase.initialize_app(config)
# Déf. authentification/firebase.
auth = firebase.auth()
# Déf. BDD/firebase
db = firebase.database()

##############################################################################

# Méthode affichage formulaire authentification.
def index(request):
    return render(request, "login.html")

# Méthode affichage formulaire d'inscription.
def register(request):
    return render(request, "register.html")

# Méthode d'inscription avec pyrebase.
def registerSubmit(request):
    email = request.POST.get('email')
    password = request.POST.get("password")
    auth.create_user_with_email_and_password(email, password)
    return redirect("/")

# Méthode Authentification avec pyrebase.
def loginSubmit(request):
    email=request.POST.get('email')
    password = request.POST.get("password")
    try:
        user = auth.sign_in_with_email_and_password(email,password)
    except:
        message = "Identifiant et/ou mot de passe invalides."
        return render(request,"login.html",{"msg":message})
    print(user)
    request.session['auth'] = True
    request.session['user'] = email

    return redirect("/home/")

def logout(request):
    request.session.flush()
    return redirect("/")

# Méthode pour afficher Home.
def home(request):

    # Check si utilisateur authentifié.
    try:
        request.session['auth']
    except:
        return redirect("/")
    
    email = request.session['user']

    return render(request, "home.html", {"e": email})    

# Méthode pour afficher Observations du Candidat
def observation(request):
    name = request.POST.get('name')
    firstname = request.POST.get('firstname')
    birthday = request.POST.get('birthday')
    date = request.POST.get('date')
    time = request.POST.get('time')
    school = request.POST.get('school')

    candidat = {
    "name" : name,
    "firstname" : firstname,
    "birthday" : birthday,
    "date" : date,
    "time" : time,
    "school" : school
    }

    db.child("candidat").push(candidat)

    #candidatKey = db.child("candidat").get()
    #print(candidatKey.key())

    try:
        request.session['auth']
    except:
        return redirect("/")    
    email = request.session['user']    

    return render(request, "observation.html", {"e": email, "name": name, "firstname": firstname})

def observationSubmit(request): 
    name = request.POST.get('name')
    firstname = request.POST.get('firstname')

    c1 = request.POST.get('c1')
    c1_comment = request.POST.get('c1_comment')
    c2 = request.POST.get('c2')
    c2_comment = request.POST.get('c2_comment')
    c3 = request.POST.get('c3')
    c3_comment = request.POST.get('c3_comment')
    c4 = request.POST.get('c4')
    c4_comment = request.POST.get('c4_comment')
    c5 = request.POST.get('c5')
    c5_comment = request.POST.get('c5_comment')
    c6 = request.POST.get('c6')
    c6_comment = request.POST.get('c6_comment')
    c7 = request.POST.get('c7')
    c7_comment = request.POST.get('c7_comment')
    c8 = request.POST.get('c8')
    c8_comment = request.POST.get('c8_comment')

    observation = {
    "c1":c1, "c1_comment":c1_comment, 
    "c2":c2, "c2_comment":c2_comment, 
    "c3":c3, "c3_comment":c3_comment, 
    "c4":c4, "c4_comment":c4_comment,
    "c5":c5, "c5_comment":c5_comment,
    "c6":c6, "c6_comment":c6_comment,
    "c7":c7, "c7_comment":c7_comment,
    "c8":c8, "c8_comment":c8_comment
    }

    db.child("observation").push(observation)

    try:
        request.session['auth']
    except:
        return redirect("/")    
    email = request.session['user']    

    # Gestion du radio
    # for i in range (len(observation)) :
      

    return render(request, "resume.html", {"e": email, "name": name, "firstname": firstname, "observation" : observation })